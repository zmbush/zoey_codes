(function () {
  function getMonthsBetween(date1, date2, roundUpFractionalMonths) {
    //Months will be calculated between start and end dates.
    //Make sure start date is less than end date.
    //But remember if the difference should be negative.
    var startDate = date1;
    var endDate = date2;
    var inverse = false;
    if (date1 > date2) {
      startDate = date2;
      endDate = date1;
      inverse = true;
    }

    //Calculate the differences between the start and end dates
    var yearsDifference = endDate.getFullYear() - startDate.getFullYear();
    var monthsDifference = endDate.getMonth() - startDate.getMonth();
    var daysDifference = endDate.getDate() - startDate.getDate();

    var monthCorrection = 0;
    //If roundUpFractionalMonths is true, check if an extra month needs to be added from rounding up.
    //The difference is done by ceiling (round up), e.g. 3 months and 1 day will be 4 months.
    if (roundUpFractionalMonths === true && daysDifference > 0) {
      monthCorrection = 1;
    }
    //If the day difference between the 2 months is negative, the last month is not a whole month.
    else if (roundUpFractionalMonths !== true && daysDifference < 0) {
      monthCorrection = -1;
    }

    return (
      (inverse ? -1 : 1) *
      (yearsDifference * 12 + monthsDifference + monthCorrection)
    );
  }

  const MPS = 1000;
  const MPM = MPS * 60;
  const MPH = MPM * 60;
  const MPD = MPH * 24;

  function writeStats(id, start_date, rounding) {
    const now = new Date();
    var months = getMonthsBetween(start_date, now, rounding);
    var delta = now - start_date;
    const days = Math.floor(delta / MPD);
    delta -= days * MPD;
    const hours = Math.floor(delta / MPH);
    delta -= hours * MPH;
    const minutes = Math.floor(delta / MPM);
    delta -= minutes * MPM;
    const seconds = Math.floor(delta / MPS);
    document.getElementById(
      id
    ).innerHTML = `<h1>${months} Months</h1> <h3>Or...</h3> ${days} Days ${hours
      .toString()
      .padStart(2, "0")}:${minutes.toString().padStart(2, "0")}:${seconds
      .toString()
      .padStart(2, "0")}`;
  }

  setInterval(function () {
    writeStats("hrtstats", new Date(2023, 2, 24, 17, 30));
    writeStats("jablog", new Date(2023, 6, 26));
    writeStats("crackstats", new Date(2022, 11, 5), true);
  }, 100);
})();
