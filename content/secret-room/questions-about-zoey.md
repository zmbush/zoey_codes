+++
in_search_index = false
title = "Hi there! I'm Zoey 💖"
template = "plain-page.html"
+++

I know this change is going to take some getting used to, so I'm going to try to
explain some things. If there are any questions you have that are not addressed
by the below, please let me know!

## What is your full name? Are you changing your middle name?

My current full name is "Zoey Michael Bush". I really wanted to keep my
initials, and keeping the middle name lets me honor where I came from, while
still being able to be more feminine.

## Can I still call you Zach? What about using he/him pronouns?

I would prefer you use my new name/pronouns.

## What's with your pronouns? What does she/they mean?

I like both the pronouns she/her _and_ they/them! You are free to use either at
any time, but if you want to make me really happy try switching back and forth!

## How do we refer to you now? Are you still our son/brother/etc?

As I consider myself neither a man or a woman, I don't have a particularly
strong reaction against male means of address. _However_ I do prefer either
feminine or gender neutral means of address. I am your
sister/sibling/daughter/child.

## Do you have any nicknames for your new name?

I'm sure more will come up over time, but at the moment, I like both "Z" or "Zo"!

## Can I tell people?

Feel free to tell your friends if you'd like. I haven't yet come out to our
extended family, and I'm not yet certain how I'd like to handle it so I'd prefer
nobody mention it to our extended family.

## Are you getting "the surgery"?

I have no current plans to.
