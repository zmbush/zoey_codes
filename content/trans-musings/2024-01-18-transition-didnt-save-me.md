+++
title = "Transition Didn't Save Me"

[taxonomies]
tags = ["trans", "transJoy", "TherapyThoughts", "positive"]

[extra]
original = "https://anarres.family/@zoey/111779833985679684"
+++

Transition did not save my life. I was not depressed, I did not feel like
something was missing or wrong, I did not need to transition in any sense of the
word. I "merely" wanted to. And this has caused me no end of worry as I fretted
over whether I will be "found out" and told I wasn't trans, or worse, that I
would grow to regret my decision and hate the "mutilated" body I inhabited.

<!-- more -->

But this was all misplaced. Polite society likes to insist that any sort of body
modification wether it be multiple piercings, or tattoos, or even gender
transition are these permanent thresholds that you can never go back from. That
this is somehow scary and should be considered with reverence and long
introspection. As though these things, visible to those around us, are the only
permanent things. Everything is permanent. I will always be trans, because the
last year of my life will always be a part of me. I will always be someone who
lived in the Bay area, someone who went to college, who lost a nephew to cancer.
These changes are no less permanent than the ink in my skin or the metal in my
ears, and yet they are seen as less of a change. Less something for which regret
is a horrible outcome. We are changed invisibly or visibly by everything we
encounter and do, and that change is permanent. Everything is permanent, and
nothing is permanent, and that is beautiful.

I inject estradiol into my leg every week, not to save my life, but to enhance
it. My life is richer for having this experience, I am privileged to experience
something that 99% of humans never even consider. And if I change my mind, my
body will be in no sense "mutilated". Because trans bodies are transcendentally
beautiful. They are a potent symbol fighting against the ways in which society
seeks to minimize all of us. Trans bodies are beautiful because they are the
outcome of profound self love and self acceptance. My body is a trans body. It
is a body I own, and one that I live in. And I am working each day to make it as
comfortable a home as I can, and that is beautiful.

I am beautiful. You are beautiful. Everything is permanent. Nothing is
permanent.

I love you
