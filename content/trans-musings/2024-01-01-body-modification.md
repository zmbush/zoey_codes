+++
title = "On Body Modification"

[taxonomies]
tags = ["trans", "transJoy"]

[extra]
original = "https://cutie.city/@zoey/111741881683291746"
+++

Getting my first tattoo has been an eye opening experience for me. I've wanted
to get a tattoo for as long as I can remember, but I never did it because I
could never decide on something that I wanted permanently on my body. What was
something that mattered so much to me that I could never regret it.

I think I had a lot of similar thoughts about transition. Sure, I may want it
now, but what if I decide I don't in a few years?

<!-- more -->

But getting this tattoo I realized, it's not about finding some perfect thing,
some perfect decision that you'll never regret. It's a story. It means something
to me right now, and so what if some hypothetical future version of me
disagrees. I don't owe future me some pristine unmodified body, and she doesn't
get to decide what I do now.

When I started to transition I was constantly terrified that I was going to find
out I'm not really trans, and that I'd have to detransition. But now I
understand:

I am trans. I will always be trans, and it is impossible for me to detransition.
Because even if I stop taking hormones, and go back to using he/him pronouns.
*This*, this experience, this community, will always be a part of me. Nobody can
ever take this away from me, as they can never take my tattoo, or my piercings.

I am trans, and I will always be trans. And that is good.
