+++
title = "But Will It Be Worth It?"

[taxonomies]
tags = ["trans", "StoryTime", "partners"]

[extra]
original = "https://cutie.city/@zoey/111314582912551121"
+++

"Is it going to be worth it?", she asks with a tinge of sadness, her voice ringing out in the quiet late night air.

The question had been asked before. A hope, perhaps, that the pain, the uncertainty, the fracturing of a potential imagined future would be worth something. Would mean something.

I can't answer.

<!-- more -->

Of course I could just say it's worth it, I could just say that I'm unambiguously happier than I've ever been. That I'm certain that this is the right choice for me.

I am not.

I can't lie. To lie would be worse than voicing the truth. "I don't know", I sigh as the admission exits my lips. The glimmer of hope on her face fades. I can't look at her. I wish I *could* say that I was sure. That would make things easier, right? But I'm not.

"That's worse!" she finally replies, the implications cutting deep.

What can I say? I don't want to stop, but would it really be so bad to go back to how things were before? We were happy. Our relationship was easier. But if I was going to stop at this point, it would be for her. My only regret in all of this is how I went about telling her, and how she reacted.

I wasn't sad, or depressed, or even numb as a guy. I was fine. But I think there was always a part of me that would prefer I be different. I looked at those that could present more femininely, soft and smooth, long hair and curves, with a tinge of longing. That could never be me I had thought. And in some ways I'm right. I'm never going to stop being 6'5", being generally large framed, I'll never pass.

But then, how do you un-ring that bell. To know that I like being perceived as feminine, to be called she, to wear women's clothing, how could I go back? She doesn't want to date a woman. She doesn't want a sexual relationship with a woman.

So what? Do I give up on being seen as feminine to keep my relationship in order? To keep my life simple and stable? Because if I want our relationship back the way it was I'd have to give that up.

She doesn't want to date a woman.

But will it be worth it?

I can't say.

"I'm sorry," I start, trying to put a voice to the swirling vortex of thoughts and emotions, "I didn't mean to hurt you. I didn't *want* to hurt you."

She looks at me again, I can't read her expression. "I know," she finally says, "but that doesn't make me feel any better. If you had done it on purpose it would be so much easier, I could just leave."

I don't know where to go from here. I can't reassure her, as I can't reassure myself.

I don't want to stop.

But will it be worth it?

I don't want to stop.
