+++
title = "Trans Musings"
page_template = "trans-musing.html"
sort_by = "date"
+++

Transition is a strange and wonderful experience. As someone who does not fit into the dominant trans narrative, I often found myself needing reassurance that simply wanting to transition is enough.
