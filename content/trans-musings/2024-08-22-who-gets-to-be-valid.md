+++
title = "Who Gets to be Valid?"

[taxonomies]
tags = ["trans", "validity", "NonBinary", "TransJoy"]

[extra]
original = "https://anarres.family/@zoey/113014044794695039"
+++

Tell me if you've heard this one before: A person is born, and assigned a gender, but from the very beginning they never act as expected. They only wanted toys and activities associated with the opposite gender, and at a very young age, some time between, say, 5 and 13 they express to their parents that they are actually *not* the gender that was assigned to them, and they will not be happy as their assigned gender. <!-- more --> Then they transition and they are able to go from infinitely miserable to infinitely happy and never have a shred of doubt.

I know I did. Countless times, in countless ways. The trans person *must* have always known. The trans person *must* have crippling dysphoria. The trans person *must* feel infinitely happier expressing their true gender. The trans person *must* have a long history of signs to point to. The trans person *must* be certain. This is what it means to be trans. And, of course, many words have been spilt trying to understand why these narrative touchpoints exist (hint: often times, [it's the gatekeepers][sgw-no-signs]).

It took a lot of talking with actual trans folks for me to realize that, no, these narrative beats don't all exist in every trans story, and that didn't make them any less trans. Of course you don't need to have always known. Of course you don't need signs all the way to childhood. But everyone has some of them, right?

Right?

I don't.

I barely thought about my gender until I was in my thirties. My dysphoria was incredibly mild, and only got a bit stronger post cracking. I was completely fine as my assigned gender. I think I could count my signs from the past on one hand. I am not certain. But I'm still trans. I've been transitioning for nearly 2 years, and on HRT for 16 months. And I like it. But [it didn't save me](@/trans-musings/2024-01-18-transition-didnt-save-me.md). I don't want to stop, but I can't say I'd rather die than detransition.

## "You Don't Need Dysphoria to be Trans"

I've heard this so many times since I started hanging in explicitly trans spaces. Since so much of our experience is often defined by suffering, decoupling dysphoria from transness is so important. If we can define ourselves by our joy instead, it's a big step away from the pathologized definitions created by cis doctors. However, I almost always see this paired with "I didn't think I had dysphoria until I cracked", and while that sentiment is likely well meaning, it serves to defeat the purpose of the initial adage. It also caused me (and I have to assume others) to internalize that "you don't need dysphoria to be trans" really meant "all trans people have dysphoria, they just might not recognize it". Which puts us right back in the zone of defining ourselves by suffering.

And dysphoria itself is treated as an almost mythic force. Uncontrollable, unpredictable. I would twist myself in knots trying to avoid this minotaur at the middle of the labyrinth of my gender, as if somehow a wrong step in my transition would lead me to some horrible mutilated body that I forever regretted. My brain had conceptualized it as something which, if I looked deep enough, I would find, and I'd never be able to ignore it.

## Imagine Just Wanting This

In the countless hours I've spent in the last two years trying desperately to understand myself, my gender, and where I'm going with all of this there has only been one constant. *I want to transition.*

But why? Where's the dysphoria? Where's the pain? How, as I've asked myself so many times, could I have reached 31 years of age and be completely fine with my gender until one day when I thought about it too hard? It became very important to me to realize that transition (especially medical transition) is just another form of [body modification](@/trans-musings/2024-01-01-body-modification.md). And as with all forms of body modification, whether it be piercings or tattoos, all that should be necessary is the want.

## Fuck It, It's Fun

As a brilliant woman once told me

> transitioning to avoid death is one thing, transitioning just to say "Fuck it, it's fun" is a WHOLE other level of Fuck the Patriarchy badass.

Cis society *needs* our existence to be defined by suffering, because to their view, they can't possibly comprehend why anyone could ever consider transition. It can't just be something fun. It can't just be something that makes us happy. It has to be pathologized and scrutinized and gatekept. I started hormones at an informed consent clinic, and still I had to fight past the mental walls put up by the cis that tell us that what we're doing must be our last ditch effort. It must be this or death.

Instead of just this.

My trans experience does not match *the narrative™* in almost any way. I learned I was trans, that I *could* transition and that was all I needed. It sounded enjoyable and exciting and fulfilling, and off I went stumbling blindly down the unmarked streets of my own genderverse.

I don't know where it is I'm going, or even why exactly I began. But I did, and I'm not stopping for anyone but myself.

[sgw-no-signs]: https://stainedglasswoman.substack.com/p/there-were-no-signs
