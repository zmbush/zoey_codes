+++
title = "New Birthdays and the Scourge of Self Doubt"

[taxonomies]
tags = ["trans", "anxiety", "SelfDoubt"]

[extra]
original = "https://cutie.city/@zoey/111237951694446870"
+++

I was talking the other day with a few folks about when they consider their "new birthday" to be.
A popular one, of course, is HRT day, and another is cracking day.
I started cracking 11 months ago, and I've been on HRT for over 6 months, but neither of those days feel as monumentally significant as it seems to be for others.
They were exciting and scary at the time, but I don't know if I could consider them to be life changing moments.
I made some comments to this effect in the conversation, then later changed my mind and deleted them.

<!-- more -->

Of course, everyone's experience and journey is different, this is a well worn phrase in our community.
And yet, I feel like things like this keep happening to me.
My experiences don't line up (especially on what seem to be touchstone moments for many) and then I'm backing away and hiding.
I feel like I'm a fraud, like I don't match up enough with others to call myself trans.

I don't think I was sad as a boy.\
I didn't think about being a girl constantly.\
I don't have all these signs to look back on.\
I don't think I have any significant trauma associated with my birth gender (or being forced to perform it).

And I know.\
I know none of these things are required to be trans.\
But it feels like maybe if I had some of them I wouldn't be fighting with myself all the time.\
Constantly trying to convince myself that I'm not a liar.\
That I'm not pretending.\
That I'm not just doing this for attention or to feel cool or included.\
I just find the idea of being a girl to be inherently appealing.

But then I'm constantly in this headspace where I'm worried I'm going to get caught.
Like I'm just doing some things that I think I'll like, trying to make adjustments to how the world sees me, but I'm terrified someone is going to come along and tell me I'm doing it wrong.
Tell me I'm a fake because *some reasons*.
Tell me that I'm spitting in the faces of *real* trans people by transitioning just because I wanted to.
Just because it seemed nice.

And, of course, I know no reasonable trans person would say those things to me.
But what if they're thinking it?
And I know this line of reasoning is so common it's practically a cliché: "Every trans person is valid except me".
But sometimes it really really feels like that for me.
I would never question anyone about who they say they are, so why would I expect anyone to question me?

Or perhaps, do I think someone is going to "catch" me, to tell me I'm not really trans, because deep deep down that's what I really think?
I think I just deep down don't believe myself.

Sometimes I do, and when I do it's nice, freeing, calming, exhilarating even.

All of my evidence feels circumstantial:

1. I want to be trans
2. I've been on HRT for 6 months and I like the changes
3. I find the idea of being seen as a girl (or at least more feminine) appealing for reasons I have never been able to articulate
4. I'm still fucking doing this after almost a year

🤷‍♀

I don't really have a point writing this, I think I just needed to get some thoughts out of my head.
If you've read this far, thank you and I love you.
