+++
title = "1 Year on HRT"

[taxonomies]
tags = ["trans", "TransTimeline", "HRT", "transfem", "enby", "nonbinary", "TransJoy"]

[extra]
original = "https://anarres.family/@zoey/112172052023230323"
+++

Last Sunday was my 1 year anniversary of starting E. It's a big milestone, a year. 365 days. 12 months. I had hoped that I'd be able to articulate some kind of sublime summation of the last year. To contextualize it. And of course I could say something pithy like "It's been an incredible journey", and while that's true, it's obviously not the whole story.

<!-- more -->

I often wish my story was more similar to other trans people's experiences or, I suppose, was more similar to the culturally "expected" trans narrative. Because fighting against the cishet hegemony is hard, and scary. And doing it because you had to, because to not do it would be a worse fate is one thing. But doing it simply because "hey, this sounds fun" is a completely different animal. I can't rely on the (more palatable to the cis) narratives of "I've always been this way" and "I didn't have any choice" because I don't believe them to be true to my experience.

My transition journey started "simply" because I thought it sounded really appealing, for reasons I couldn't (and still can't) articulate. Up until about a year ago, I'd honestly just described myself as agender, because I really had no strong attachment either way. But then the idea that one could transition not because of distress and need, but for joy and want was presented to me. And it proved too intriguing to pass up.

I've long considered queer people, especially the visibly queer, to be absolutely incredible. To be so unapologetically yourself, was something I always looked on at in awe. And in the last year, I've slowly realized how much my worry about rejection or being mocked and shunned deeply affected my interface with the world. I wanted the colored hair. The piercings. The wild clothes. And that's now within my grasp. Each day I'm getting closer to becoming the queer I always admired.

I love the feeling of my changing body, the friends I've made, the things I've learned. I love my tattoo, my piercings, my purple hair. I continue to look forward to becoming a gayer version of myself. As I've said many times to myself and others over the last year: "I don't know where I'm going, but I like where I've gone so far!"

![1 Year on HRT Timeline](1-year-on-hrt.png "Timeline of 1 year on HRT")
