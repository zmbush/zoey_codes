+++
title = "How Did I Get Here?"

[taxonomies]
tags = ["trans", "anxiety", "lyrics"]

[extra]
original = "https://cutie.city/@zoey/111399225000984513"
+++

I like being trans. I like being girl adjacent.

At least that's what I tell myself.

But if I really was trans, if I really was a girl, then how did I get to be 30 years old and not have any significant gender trauma? How did I barely think about it for decades?

<!-- more -->

My cracking wasn't a bolt from the blue, realizing that all of my life suddenly made sense now. It was seeing messages like "if you want this, you can have it" and thinking "yes, that sounds awesome." It wasn't relief, it was excitement. But it was an anxious excitement.

And I jumped in.

But maybe I jumped in too fast. My partner certainly thought (thinks) so. I told her I thought I was trans only a couple weeks after I started considering the possibility. She did not take it well. Assuming this was some convoluted way to get divorced. Saying I must have hated our relationship, that I was going to change and leave.

And yet I continued. I started HRT a couple months later because I really wanted to know how I'd feel on E.

A misunderstanding. I am not sure if I want children. She's sure that she does. I would rather adopt than have biological. She disagrees. Sees me starting hormones as taking a choice away from her. From us. For making a decision for us that should have been together.

But I continued. Because I'm selfish? Sometimes it feels that way. I wanted to see what would happen if I continued. I liked the changes, so that must've meant something, right?

She doesn't like the clothes I pick, and she tells me that. She doesn't like makeup, and doesn't like how I look when I wear it, and she tells me that. She hates my voice when I try voice training, tells me it's like nails on a chalkboard. She hates change, sure, but is that it?

I try to give her space. Try to not talk about transition or the folks I'm talking to, because it seems to upset her. But still I continue.

Do I continue because I like this, or because I don't want to be wrong? Because I don't want the strife, and irreparable damage to my relationship to be for nothing? I can't tell.

Sure, folks say that a cis person on HRT would feel terrible, but I have a hard time believing that. Maybe some would, but most of the changes from feminizing HRT are just objectively better, right?

I wasn't miserable, or even sad as a guy. I don't have trauma associated with it. I don't feel infinitely happier now, but I do feel hopeful. I do look into the mirror and see a more feminine face, and smile.

But is this who I really am?

My partner says our 10 year relationship was a lie. That I was a lie. That I catfished her. I know she's hurt, and doesn't mean all these things. But there's a kernel of truth there.

She thinks I've always known. That I just stayed with her because it was easy, the "correct" thing to do.

I tell her I never even realized it was an option. I say society made sure that from a young age I knew that trans people were either crazy, evil, or pitiable. She asks when I've ever let society tell me what to do. This feels different.

I hate that I've hurt my partner. In ways that feel irreparable. In ways that seem callous.

She says she asked for compromise and that I ignored her.

But I don't remember calls for compromise.

* I remember her saying she hates my voice
* I remember her saying "don't be stupid and come out"
* I remember her criticizing my every choice of fashion
* I remember her being so miserable that I preferred not having a beard

Maybe she asked, and I don't remember. She does have a better memory than me. But because of her uncharitable initial reaction, my brain classified her as an enemy. That she was against me. That she wanted nothing more than for me to change my mind.

I don't feel like I'm running from trauma. The joy I am running towards feels small and far away.

I am selfish. I am callous. I am cruel. I do things without considering how they affect others, especially my partner.

Why not just stop? The experiment has gone on long enough, hasn't it? Can this really be worth it?

My partner tells me that she loves me, but isn't in love with me. That I'm not her type anymore. I say the same, because I'm tired. Tired of fighting. Tired of hoping. Tired of waiting for her to come around.

I'm scared, but reminded of these lyrics:

🎵It's the feeling you get when you've got a crush
Or at the edge of the cliff before you take the plunge
A little bit hopeful, a little bit scared🎵

Thank you for reading this far 💜
