+++
title = "Am I Allowed to Be Angry?"

[taxonomies]
tags = ["trans", "therapy", "partners"]

[extra]
original = "https://anarres.family/@zoey/113343457022738682"
+++

Emotions are complicated. I've never been particularly good at understanding, or feeling, or expressing my own emotions. But the emotion I perhaps have the most uncomfortable relationship with is anger. I was an angry child. Not in the sense that I was angry at the world, or upset all the time, but in the sense that I had a *very* short fuse. My sister knew this, and delighted in finding ways to trigger my very explosive rage. <!-- more --> Screaming bloody murder at her for the smallest of slight was not uncommon for much of my childhood. She once told me, many years later, that she was concerned for my health back then because of how easily and violently I would get angry.

But at some point, I don't even recall when, I stopped.

I stopped getting angry. I didn't explode in screaming fits. I didn't fight with my sister. I had, somehow, "turned off" my rage. And for a long time, I actually started to believe that I couldn't get angry. I was reasonable, I was calm, I could see where the other side was coming from. I used to see it as a point of pride. I didn't get into fights, my partner and I for 10+ years never had a fight beyond a mildly tense discussion.

But I didn't really turn it off. In some cases I just stopped caring as much (it's a lot easier to concede a point when you don't care about the outcome), in others my methods of expressing anger had narrowed down to curt responses or pointed silence. But still, I wasn't screaming and slamming doors, so not really angry, right?

And then I started to think I might be trans, and tried to introduce the idea to my partner. She doesn't like change, and I'm kinda vague describing how I'm feeling, so she doesn't take it well. Assumes I'll leave her and that I really want to marry a man instead. Okay, understandable, it's a big thing to consider out of nowhere.

And then I consider starting hormones and the topic of kids come up, I don't recall ever saying concretely that I wanted kids, but she remembers differently. Maybe I was non-committal and didn't make it clear how unsure I was. I ultimately decide if I want kids I had no attachment to them being biologically "mine" so banking sperm seemed like an unnecessary expense.

I start hormones and she realizes that even though she thought she was pan "in theory", that she's definitely straight, or at the very least was not attracted to me being more fem. That's fine, I don't begrudge her sexual orientation. It's not like she has a choice in the matter.

I try to find clothes, and makeup, and maybe get excited about the changes that HRT is bringing. But she won't help find clothes. And she hates makeup, only able to see it as a method of oppression. And she cannot be excited for any of the changes I'm enjoying. She didn't sign up for this. She's never been a girly girl. She didn't sign up for this. I can't expect her to be excited when she can only see it as the person she loved disappearing slowly in front of her.

She's "fine" with the changes.

She wants it to be over so we can "move on".

She says being trans is my whole life.

I try to practice makeup and it's a discussion about how I don't understand what it means to wear makeup. How I'm enforcing a narrow definition of femininity. How I'm looking at it "like a man would"

I find some clothes, some of which I like a lot. All she can say is how they don't fit me right. I tear a hole in a pair of jeans, she says it's because I should just wear men's jeans. That they look the same. That they tore because they "don't fit my body right".

I try to avoid having her see me in makeup or new outfits. Because she always has a comment, or a weird face.

I am angry at my partner.

I am angry because she is unable or unwilling to avoid projecting her own issues with femininity onto me. I am angry because she cannot bring herself to be excited for me. I am angry because she refers to the last decade as me lying to her. I am angry because she thinks I'll leave her for the first kind look I receive. I am angry because still hasn't updated my fucking name in her phone.

I don't feel like I deserve to be angry with her. After all, I didn't need to transition. I was not miserable. I didn't constantly feel like something was missing. I just thought it sounded like fun, and I felt deep down that if I didn't give it a try that I would resent her. Because I'm impatient, and I can't wait when I have a new toy I want to play with. Because I am single-minded, and when a new obsession comes along it becomes all that matters to me. I'm not a totally different person now. I'm not suddenly able to smile.

It shouldn't matter this much. To me, or to her. But I broke something between us, and I don't know how to fix it.

My therapist asked me a few months ago: if my partner had come out to me as trans, how would I have reacted? I can't believe it'd be like this. It my partner came out as trans I would be so fucking excited for them. I would celebrate every little change alongside them. I wholeheartedly believe this is how I would have reacted.

So is it so terrible that I'm angry at her because she can't do the same?
