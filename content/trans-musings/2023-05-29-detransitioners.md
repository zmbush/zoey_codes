+++
title = "Detransition Fears"

[taxonomies]
tags = ["trans", "detransition", "anxiety"]

[extra]
original = "https://eldritch.cafe/@zo/110451071205643981"
+++

I watched a video earlier this week from a detransitioner and it's been messing with my head.

I think I want to be a girl. I think I want to wear feminine clothes. I think I want to be perceived as female.

<!-- more -->

I know I like to be called Zoey, to be called she, to be called aunt. I find the idea of feminine clothes appealing, and I like the way they feel. But when I see myself wearing them, it looks wrong. Maybe it's because with everything I've tried, I can't see myself as anything but a man wearing women's clothing. And I know I don't want that.

I want to be trans.

I've held onto that thought, for months. That's got to mean something right? A cis person wouldn't want so hard to be trans. Wouldn't spend months scouring their personal history and feelings for signs.

But I did. And I found nothing. Or rather almost nothing. A few comments over the years. Some fear about increased body hair as I age. Fond memories of wearing my girlfriend's shorts, or my sister's clothes once or twice.

And then I look for euphoria, and it feels like there's very little there as well. I smile every time someone calls me 'she'. I like the way it feels to be tucked, and I've been wearing a bra every day for six months. I get excited when I buy and try on clothes. But it feels so mild. Transient.

I've been on HRT for two months, and I've been happy for or ambivalent to all of the changes. I'm loving the softer skin, less BO, and less greasy hair. All of the potential changes still sound really appealing, which they wouldn't to a cis person, right?

I wish all of this didn't feel so dangerous. I wish I could feel more certain. I wish society didn't care about gender. I wish I didn't care about gender.
